from django.contrib import admin
# Using the import keyword allows access to the include function to allow use of the URLs/routes created in the previous step
# The include() function allows referencing other URLconfs. Whenever Django encounters include(), it chops off whatever part of the URL matched up to that point and sends the remaining string to the included URLconf for further processing
# The idea behind include() is to make it easy to plug-and-play URLs. Since todolist are in their own URLconf (todolist/urls.py), they can be placed under “/todolist/” or under “/content/todolist/”, or any other path root, and the app will still work
# You should always use include() when you include other URL patterns. admin.site.urls is the only exception to this
from django.urls import include, path

urlpatterns = [
    path('todolist/', include('todolist.urls')),
    path('admin/', admin.site.urls),
]
