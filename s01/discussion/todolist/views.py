from django.shortcuts import render, redirect, get_object_or_404
# The from keyword allows importing of necessary Classes, methods and other items needed in our application from the "django.http" package while the "import" keyword defines what we are importing from the package
# from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password

# Local Imports
from .models import ToDoItem
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm

def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)

    context = {
        "todoitem_list": todoitem_list,
        "user": request.user
    }
    return render(request, "todolist/index.html", context)

def todoitem(request, todoitem_id):
    # The model_to_dict method allows to convert models into dictionaries
    # The .get() Model method allows to retrieve a specific record using it's primary key (pk)
    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    # Instead of creating a context, we can pass the dictionary directly as the context of the view
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def register(request):

    context = {}

    if request.method == 'POST':

        form = RegisterForm(request.POST)

        if form.is_valid() == False:

            form = RegisterForm()

        else:

            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']

            duplicates = User.objects.filter(email=email)
            
            # if todoitem does not contain any duplicates
            if not duplicates and password == confirm_password:

                # Creates an object based on the "ToDoItem" model and saves the record in the database
                User.objects.create(username=username, first_name=first_name, last_name=last_name, email=email, password=make_password(password), is_staff=False, is_active=True)
                return redirect("todolist:login")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/register.html", context)

def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="johndoe", password="john1234")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    return render(request, "todolist/change_password.html", context)

def login_view(request):

    context = {}

    # If this is a POST request we need to process the form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request
        form = LoginForm(request.POST)

        # Check whether the data is valid
        # Runs validation routines for all its fields and returns True and places the form's data in the "cleaned_data" attribute
        if form.is_valid() == False:

            # Returns a blank login form
            form = LoginForm()

        else:

            # Retrieves the information from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }
            
            if user is not None:
                # Saves the user’s ID in the session using Django's session framework
                login(request, user)
                return redirect("todolist:index")
            else:
                # Provides context with error to conditionally render the error message
                context = {
                    "error": True
                }

    return render(request, "todolist/login.html", context)

def logout_view(request):
    logout(request)
    return redirect("todolist:index")

def add_task(request):

    context = {
        "user": request.user
    }

    if request.method == 'POST':

        form = AddTaskForm(request.POST)

        if form.is_valid() == False:

            form = AddTaskForm()

        else:

            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            # Checks the database if a task already exists
            # By default the "filter" method searches for records that are case insensitive
            duplicates = ToDoItem.objects.filter(task_name=task_name, user_id=request.user.id)
            
            # if todoitem does not contain any duplicates
            if not duplicates:

                # Creates an object based on the "ToDoItem" model and saves the record in the database
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):

    # Returns a queryset
    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        # Accessing the first element is necessary because the "ToDoItem.objects.filter()" method returns a queryset
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':

        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:

            form = UpdateTaskForm()

        else:

            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']
            
            if todoitem:

                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                return redirect("todolist:index")

            else:

                context = {
                    "error": True
                }

    return render(request, "todolist/update_task.html", context)

def delete_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
    return redirect("todolist:index")