from django.db import models
from django.contrib.auth.models import User

# [Section] Models
# Each model is represented by a class that subclasses django.db.models.Model. Each model has a number of class variables, each of which represents a database field in the model
# Each field is represented by an instance of a Field class – e.g., CharField for character fields and DateTimeField for datetimes. This tells Django what type of data each field holds.
# The name of each Field instance (e.g. task_name or date_created) is the field’s name, in machine-friendly format. You’ll use this value in your Python code, and your database will use it as the column name.
# Some Field classes have required arguments. CharField, requires that you give it a max_length. That’s used not only in the database schema, but in validation, as we’ll soon see.
# A Field can also have various optional arguments; in this case, we’ve set the default value of status to "pending.
class ToDoItem(models.Model):
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="pending")
    date_created = models.DateTimeField('date created')
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")

class Event(models.Model):
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="pending")
    date_created = models.DateTimeField('date created')
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")

