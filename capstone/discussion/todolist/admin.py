from django.contrib import admin

from .models import ToDoItem, Event

admin.site.register(ToDoItem)
admin.site.register(Event)
